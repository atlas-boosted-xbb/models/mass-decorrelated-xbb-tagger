# Mass-decorrelated-Xbb-tagger
Tools for training and study of mass de-correlated Xbb tagger using deep neural network
by Wei Ding 
wei.ding@cern.ch .

1, reweight
2, process
3, prepare
4, train
5, study

The instructions for how to run are in the file run.sh in each directory.
This tool is inherited from the tool by Julian Collado Umana 
https://gitlab.cern.ch/atlas-boosted-hbb/xbb-tagger-training.  
And more studies of this tagger are here:
https://indico.cern.ch/event/864911/ . 


